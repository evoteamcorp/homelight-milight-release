import { Network } from "./Network/Network";
import { Color } from "home-light/internals/Color";
import { IDevice } from "home-light/interfaces/IDevice";
export declare class Device implements IDevice {
    ip: string;
    macAddress: string;
    type: string;
    private _network;
    constructor(ip: string, mac: string, network: Network);
    on(zone?: number): void;
    off(zone?: number): void;
    setBrightness(percent: number, zone?: number | Array<number>): void;
    setColor(color: Color, zone?: number | Array<number>): void;
    setWhite(zone?: number | Array<number>, brightness?: number): void;
    setNightMode(zone?: number | Array<number>): void;
}
