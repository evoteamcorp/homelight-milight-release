export interface IDictionnaryZone<T> {
    ALL_ZONES: T;
    ZONE_1: T;
    ZONE_2: T;
    ZONE_3: T;
    ZONE_4: T;
    PARAM_1: T;
    PARAM_2: T;
    [index: string]: T;
}
export interface IDictionnaryBrightness<T> {
    PERCENT_5: T;
    PERCENT_10: T;
    PERCENT_15: T;
    PERCENT_20: T;
    PERCENT_25: T;
    PERCENT_30: T;
    PERCENT_35: T;
    PERCENT_40: T;
    PERCENT_45: T;
    PERCENT_50: T;
    PERCENT_55: T;
    PERCENT_60: T;
    PERCENT_65: T;
    PERCENT_70: T;
    PERCENT_75: T;
    PERCENT_80: T;
    PERCENT_85: T;
    PERCENT_90: T;
    PERCENT_95: T;
    PERCENT_100: T;
    [index: string]: T;
}
