import { EventEmitter } from "events";
import { Device } from "../Device";
export declare namespace PACKETS {
    const LINK_WIFI: string;
    namespace ON {
        const ALL_ZONES: number;
        const ZONE_1: number;
        const ZONE_2: number;
        const ZONE_3: number;
        const ZONE_4: number;
        const PARAM_1: number;
        const PARAM_2: number;
    }
    namespace OFF {
        const ALL_ZONES: number;
        const ZONE_1: number;
        const ZONE_2: number;
        const ZONE_3: number;
        const ZONE_4: number;
        const PARAM_1: number;
        const PARAM_2: number;
    }
    namespace BRIGHTNESS {
        const PERCENT_5: number;
        const PERCENT_10: number;
        const PERCENT_15: number;
        const PERCENT_20: number;
        const PERCENT_25: number;
        const PERCENT_30: number;
        const PERCENT_35: number;
        const PERCENT_40: number;
        const PERCENT_45: number;
        const PERCENT_50: number;
        const PERCENT_55: number;
        const PERCENT_60: number;
        const PERCENT_65: number;
        const PERCENT_70: number;
        const PERCENT_75: number;
        const PERCENT_80: number;
        const PERCENT_85: number;
        const PERCENT_90: number;
        const PERCENT_95: number;
        const PERCENT_100: number;
    }
    namespace ZONES {
        namespace WHITE {
            const ALL_ZONES: number;
            const ZONE_1: number;
            const ZONE_2: number;
            const ZONE_3: number;
            const ZONE_4: number;
        }
        namespace NIGHT {
            const ALL_ZONES: number;
            const ZONE_1: number;
            const ZONE_2: number;
            const ZONE_3: number;
            const ZONE_4: number;
        }
    }
}
export declare namespace EVENTS {
    const DISCOVER_DEVICE: string;
    const SEND_ERROR: string;
    const SEND_SUCCESS: string;
}
export declare class Network extends EventEmitter {
    private _socket;
    private _broadcastAddress;
    private _waitingOps;
    private _interval;
    private _limit;
    private _socketState;
    private _noLatencyMode;
    constructor();
    searchDevices(): void;
    send(device: Device, ...commands: Array<number>): void;
    setNoLatencyMode(state: boolean): void;
    private _send();
    private _onSocketError;
    private _onSocketMessage;
    private _onListening;
    private _getBroadcastAddresses();
    private _ensureQueue();
}
