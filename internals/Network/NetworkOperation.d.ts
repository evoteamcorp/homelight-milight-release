import { Device } from "../Device";
export declare class NetworkOperation {
    private _device;
    private _datas;
    constructor(device: Device, datas: Buffer);
    getDatas(): Buffer;
    getDevice(): Device;
}
