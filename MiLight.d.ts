import { IConnector } from "home-light/interfaces/IConnector";
import { EventEmitter } from "events";
export declare class MiLight extends EventEmitter implements IConnector {
    private _devices;
    private _network;
    constructor();
    getName(): string;
    searchDevices(): void;
    setNoLatencyMode(state: boolean): void;
    private _onDiscoveredDevice;
    private _checkIfDeviceExists(device);
}
